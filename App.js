import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from './src/menu/menu';
import ContagemCaracteres from './src/pages/contagemCaracteres';
import ContagemCliques from './src/pages/contagemCliques';
import ListaTextos from './src/pages/listaTextos';
import IdiomasDiferentes from './src/pages/IdiomasDiferentes';
import PaginaVazia from './src/pages/paginaVazia';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <StatusBar style="auto" />
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu}/>
        <Stack.Screen name="ContagemCaracteres" component={ContagemCaracteres}/>
        <Stack.Screen name="ContagemCliques" component={ContagemCliques}/>
        <Stack.Screen name="ListaTextos" component={ListaTextos}/>
        <Stack.Screen name="IdiomasDiferentes" component={IdiomasDiferentes}/>
        <Stack.Screen name="PaginaVazia" component={PaginaVazia}/>
      </Stack.Navigator>
    </NavigationContainer>
      
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

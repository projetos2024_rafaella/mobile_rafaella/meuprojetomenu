import { StyleSheet, Text, View, TouchableOpacity, Button, TextInput } from 'react-native';
import { useState } from "react";


function ContagemCaracteres ({navigation}) {
    const [text, setText] = useState("");
    const [numLetras, setNumLetras] = useState(0);

    function click(){
        setNumLetras(text.length)
    }
    return (
      <View style={styles.container}>
        <Text style={styles.text}>Digite aqui...</Text>
        <TextInput
        style={styles.input}
        placeholder="Escreva e conte seus caracteres!"
        value={text}
        onChangeText={(textInput) => setText (textInput)}
        />
        <Button 
        style={styles.button}
        title="Contar" 
        onPress={() => {
            click();
        }}/>
        {numLetras > 0 ? <Text>{numLetras}</Text> : <Text></Text>}

        <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Text style={styles.textButton}>Voltar para o menu</Text>
        </TouchableOpacity>
      </View>
      )
    }

    export default ContagemCaracteres;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
        input: {
            margin: 20,
            fontSize: 17, 
            borderColor: '#D0D9D4', 
            borderWidth: 1,
            width: "80%",
            padding: 10,
            marginVertical: 10
          },
          back: {
            backgroundColor: "#088FFE",
            padding: 10,
            borderRadius: 7,
          },
          textButton: {
            color: "#FFFAFA",
            fontWeight: 'bold'
          }
    });
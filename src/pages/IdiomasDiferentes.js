import { StyleSheet, Text, View, TouchableOpacity, Button, TextInput, ScrollView } from 'react-native';


function IdiomaDiferentes ({navigation}) {

    return (
      <View style={styles.container}>
        <Text style={styles.saudacao}>Olá mundo</Text>
        <Text style={styles.saudacao}>Hello World</Text>
        <Text style={styles.saudacao}>Hallo Welt</Text>
        <Text style={styles.saudacao}>Bonjour le monde</Text>

        <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Text style={styles.textButton}>Voltar para o menu</Text>
        </TouchableOpacity>
      </View>
      )
    }

    export default IdiomaDiferentes;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
          back: {
            backgroundColor: "#088FFE",
            padding: 10,
            borderRadius: 7,
          },
          textButton: {
            color: "#FFFAFA",
            fontWeight: 'bold'
          },
          listText: {
            fontWeight: 'bold',
            color: "#000", 
            margin: 5,
          }
    });
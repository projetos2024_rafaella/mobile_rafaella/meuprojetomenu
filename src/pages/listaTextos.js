import { StyleSheet, Text, View, TouchableOpacity, Button, TextInput, ScrollView } from 'react-native';
import { useState } from "react";


function ListaTextos ({navigation}) {

    return (
      <View style={styles.container}>
        <ScrollView>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>
            <Text style={styles.listText}>Rafaella linda</Text>

        </ScrollView>

        <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Text style={styles.textButton}>Voltar para o menu</Text>
        </TouchableOpacity>
      </View>
      )
    }

    export default ListaTextos;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
          back: {
            backgroundColor: "#088FFE",
            padding: 10,
            borderRadius: 7,
          },
          textButton: {
            color: "#FFFAFA",
            fontWeight: 'bold'
          },
          listText: {
            fontWeight: 'bold',
            color: "#000", 
            margin: 5,
          }
    });
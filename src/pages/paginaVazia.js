import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

function PaginaVazia ({navigation}) {

    return (
      <View style={styles.container}>
        
        <TouchableOpacity style={styles.button}>
            <Text style={styles.textButton}>Inativo</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Text style={styles.textButton}>Voltar para o menu</Text>
        </TouchableOpacity>
      </View>
      )
    }

    export default PaginaVazia;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
          back: {
            backgroundColor: "#088FFE",
            padding: 10,
            borderRadius: 7,
          },
          textButton: {
            color: "#FFFAFA",
            fontWeight: 'bold'
          },
          button: {
            padding: 5,
            backgroundColor: '#DB488B', 
            borderRadius: 7
          },
          textButton: {
            color: '#FFFAFA',
            fontWeight: 'bold'
          }
    });
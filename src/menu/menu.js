import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ContagemCaracteres')}>
      <Text style={styles.text}>Contagem de caracteres</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ContagemCliques')}>
      <Text style={styles.text}>Contagem de cliques</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ListaTextos')}>
      <Text style={styles.text}>Lista de Textos</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('IdiomasDiferentes')}>
      <Text style={styles.text}>Saudações pelo mundo</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('PaginaVazia')}>
      <Text style={styles.text}>Página vazia</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  button: {
    padding: 10, 
    backgroundColor: '#8747FE', 
    borderRadius: 7,
    minWidth: 200,
  }, 
  text: {
    fontWeight: 'bold',
    color: "#FFFAFA", 
    textAlign: 'center'
  }
});
